package com.in28minutes.springboot.myfirstwebapp.todo;

import jakarta.validation.Valid;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.time.LocalDate;

@Controller
@SessionAttributes("name")
public class TodoControllerJpa {

    private final TodoRepository todoRepository;

    public TodoControllerJpa(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    // /list-todos endpoint
    @GetMapping("/list-todos")
    public String listAllTodos(ModelMap model) {
        model.addAttribute("todos", todoRepository.findByUserName(getLoggedInUserName(model)));
        return "listTodos";
    }

    @GetMapping("/add-todo")
    public String showAddTodoView(ModelMap model) { //Binding from Controller to View
        model.put("todo", new Todo(0, getLoggedInUserName(model), "", LocalDate.now(), false));
        return "todo";
    }

    @PostMapping("/add-todo")
    public String addNewTodo(ModelMap model, @Valid Todo todo, BindingResult result) { //Binding from View to Controller

        if (result.hasErrors())
            return "todo";

        todo.setUserName(getLoggedInUserName(model));
        todoRepository.save(todo);  //  Saving the user data into the datastore,H2 database in this case.
        // When you want to redirect to a specific URL, specify the name of the URL, not the name of JSP file.
        return "redirect:list-todos";
    }

    @GetMapping("/delete-todo")
    public String deleteTodo(@RequestParam int id) {
        todoRepository.deleteById(id); //   Deleting from the datastore, H2 Database in this case.
        return "redirect:list-todos";
    }

    @GetMapping("/update-todo")
    public String showUpdateTodoView(@RequestParam int id, ModelMap model) {
        model.addAttribute("todo", todoRepository.findById(id).orElseThrow());
        return "todo";
    }

    @PostMapping("/update-todo")
    public String updateTodo(ModelMap model, @Valid Todo todo, BindingResult result) {

        if (result.hasErrors())
            return "todo";

        todo.setUserName(getLoggedInUserName(model));
        todoRepository.save(todo);
        return "redirect:list-todos";
    }

    private static String getLoggedInUserName(ModelMap model) {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}
