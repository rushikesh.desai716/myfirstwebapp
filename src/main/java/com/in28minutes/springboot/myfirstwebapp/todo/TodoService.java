package com.in28minutes.springboot.myfirstwebapp.todo;

import jakarta.validation.Valid;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class TodoService {

    private static int todoCount = 0;
    private static List<Todo> todos = new ArrayList<>(List.of(
            new Todo(++todoCount, "in28minutes", "Learn AWS 1", LocalDate.of(2023, 9, 16), false),
            new Todo(++todoCount, "in28minutes", "Learn DevOps 1", LocalDate.of(2024, 5, 25), false),
            new Todo(++todoCount, "in28minutes", "Learn Full Stack 1", LocalDate.of(2025, 2, 27), false)
    ));

    public List<Todo> findByUserName(String userName) {
        return todos.stream().filter(todo -> todo.getUserName().equalsIgnoreCase(userName)).sorted().toList();
    }

    public void insertTodo(String userName, String description, LocalDate date, boolean done) {
        todos.add(new Todo(++todoCount, userName, description, date, done));
    }

    public void deleteById(int id) {
        todos.removeIf(todo -> todo.getId() == id);
    }

    public Todo findById(int id) {
        return todos.stream().filter(todo -> todo.getId() == id).findFirst().get();
    }

    public void updateTodo(@Valid Todo todo) {
        deleteById(todo.getId());
        todos.add(todo);
    }
}
