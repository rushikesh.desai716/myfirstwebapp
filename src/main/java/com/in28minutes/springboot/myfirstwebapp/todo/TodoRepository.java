package com.in28minutes.springboot.myfirstwebapp.todo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface TodoRepository extends JpaRepository<Todo, Integer> {

    List<Todo> findByUserName(String userName);

    List<Todo> findByDescription(String description);

    List<Todo> findByTargetDate(LocalDate targetDate);

    List<Todo> findByDone(boolean done);

}
